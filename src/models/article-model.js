const Mongoose = require('mongoose')
const { Schema } = Mongoose

const articleSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    thumbnail: {
      type: String,
    },
    description: {
      type: String,
    },
    content: {
      type: String,
    },
    category: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    },
    view: {
      type: Number,
      default: 0
    },
    sumRating: {
      type: Number,
      default: 0,
      get: value => value.toFix(1)
    }
  },
  { 
    timestamps: true,
    versionKey: false
  }
)

articleSchema.index({ title: 'text' })

module.exports = Mongoose.model("Article", articleSchema)

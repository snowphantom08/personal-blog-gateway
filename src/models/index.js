const User = require('./user-model')
const Article = require('./article-model')
const Category = require('./category-model')
const Comment = require('./comment-model')

module.exports = {
  User,
  Article,
  Category,
  Comment
}

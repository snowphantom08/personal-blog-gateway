const Mongoose = require('mongoose')
const { Schema } = Mongoose

const commentSchema = new Schema(
  {
    user: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    article: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'Article'
    },
    content: {
      type: String,
    },
    rating: {
      type: Number,
      default: 0
    }
  },
  { 
    timestamps: true,
    versionKey: false
  }
)

module.exports = Mongoose.model("Comment", commentSchema)

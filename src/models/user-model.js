const Mongoose = require('mongoose')
const { EMAIL_REGEX } = require('../utils/email-util')
const { Schema } = Mongoose

const userSchema = new Schema(
  {
    email: {
      type: String,
      match: EMAIL_REGEX,
      required: true
    },
    password: {
      type: String,
      required: true,
      min: 6,
      select: false
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    avatar: {
      type: String
    },
    role: {
      type: String,
      enum: ['ADMIN', 'USER'],
      default: 'USER'
    }
  },
  { 
    timestamps: true,
    versionKey: false
  }
)

module.exports = Mongoose.model("User", userSchema)

const { UserController } = require('../controllers');
const { AuthMiddleware } = require('../middlewares');

const router = require('express').Router()

router.post('/login', UserController.login)

// Profile
router.get('/profile', AuthMiddleware.verifyUser, UserController.getProfile)
router.put('/profile', AuthMiddleware.verifyUser, UserController.updateProfile)

// Sign up
router.post('/signup', UserController.signUp)

module.exports = router;

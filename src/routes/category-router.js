const { CategoryController } = require('../controllers')
const { AuthMiddleware }  = require('../middlewares')

const router = require('express').Router()

router
  .route('/')
  .post(AuthMiddleware.verifyAdmin, CategoryController.newCategory)
  .get(CategoryController.searchCategory)
  .put(AuthMiddleware.verifyAdmin, CategoryController.updateCategory)
  .delete(AuthMiddleware.verifyAdmin, CategoryController.removeCategory)

router.get('/:_id', CategoryController.getCategory)

module.exports = router
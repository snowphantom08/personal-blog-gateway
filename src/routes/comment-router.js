const { CommentController } = require('../controllers')
const { AuthMiddleware } = require('../middlewares')

const router = require('express').Router()

router.get('/', CommentController.fetchComment)
router.post('/', AuthMiddleware.verifyUser, CommentController.newComment)
router.put('/', AuthMiddleware.verifyUser, CommentController.updateComment)
router.delete('/', AuthMiddleware.verifyAdmin, CommentController.removeComment)

module.exports = router
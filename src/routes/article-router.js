const { ArticleController } = require('../controllers')
const { AuthMiddleware } = require('../middlewares')

const router = require('express').Router()

// CRUD
router.get('/', ArticleController.searchArticle)
router.get('/:_id', ArticleController.getArticle)
router.post('/', AuthMiddleware.verifyAdmin, ArticleController.newArticle)
router.put('/', AuthMiddleware.verifyAdmin, ArticleController.updateArticle)
router.delete('/', AuthMiddleware.verifyAdmin, ArticleController.removeArticle)

// View article
router.get('/view/:_id', ArticleController.viewArticle)

module.exports = router
const { FileController } = require('../controllers')
const { AuthMiddleware } = require('../middlewares')

const router = require('express').Router()

router.post('/', AuthMiddleware.verifyUser, FileController.uploadFile)

module.exports = router
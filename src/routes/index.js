const router = require('express').Router()
const userRouter = require('./user-router')
const articleRouter = require('./article-router')
const categoryRouter = require('./category-router')
const commentRouter = require('./comment-router')
const fileRouter = require('./file-router')

// const { ErrorHandleMiddleware, ErrorLogsMiddleware } = require('../middlewares')

router.use('/user', userRouter)
router.use('/article', articleRouter)
router.use('/category', categoryRouter)
router.use('/comment', commentRouter)
router.use('/file', fileRouter)

// router.use([
//   ErrorLogsMiddleware,
//   ErrorHandleMiddleware
// ])

module.exports = router;

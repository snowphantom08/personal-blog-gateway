class NotFoundException extends Error {
  constructor(msg) {
    super(msg);
  }

  
}

module.exports = NotFoundException
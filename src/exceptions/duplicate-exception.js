class DuplicateException extends Error {
  constructor(msg) {
    const exceptionMsg = (msg ? `'${msg}'` : 'Some data') 
      + ' is already exist in system.';

    super(exceptionMsg)
  }

  
}

module.exports = DuplicateException
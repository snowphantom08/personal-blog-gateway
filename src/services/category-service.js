const { ObjectId } = require('bson')
const { Category } = require('../models');
const { getObjectId } = require('../utils/common-util');

  const upsertCategory = async (category) => {
    category._id = category._id || new ObjectId()

    category = await Category.findOneAndUpdate(
      { _id: category._id },
      {
        $set: category
      },
      {
        upsert: true,
        new: true,
      }
    )

    return category && category.toObject()
  }

const removeCategory = async (category) => {
  const result = await Category.remove({ _id: category._id })
  return result
}

const fetchCategory = async ({ query, offset, limit }) => {
  let filter = {}
  offset = offset || 0
  limit = limit || 100

  if (query) {
    filter = {...filter, $text: { $search: query }}
  }

  const result = await Category
    .find(filter)
    .skip(offset)
    .limit(limit)
    .sort({ createdAt: -1 })

  return result;
}

const getCategory = async (data) => {
  const id = getObjectId(data)
  const result = await Category
    .findOne({ _id: id })
  
  return result && result.toObject()
}

module.exports = {
  upsertCategory,
  removeCategory,
  fetchCategory,
  getCategory,
}
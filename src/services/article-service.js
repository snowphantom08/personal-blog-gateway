const { ObjectId } = require("bson")
const CommentService = require("./comment-service")
const { Article } = require("../models")
const { getObjectId } = require("../utils/common-util")

const upsertArticle = async (data) => {
  data._id = data._id || new ObjectId()

  data = await Article.findOneAndUpdate(
    { _id: data._id },
    {
      $set: data
    },
    {
      upsert: true,
      new: true,
    }
  )

  return data && data.toObject()
}

const removeArticle = async (data) => {
  const id = data && data._id
  const result = await Article.remove({ _id: id })
  return result
}

const fetchArticle = async ({ query, category, offset, limit }) => {
  let filter = {}
  offset = offset || 0
  limit = limit || 100

  if (query) {
    filter = {...filter, $text: { $search: query }}
  }

  if (category) {
    filter = {...filter, category: getObjectId(category) }
  }

  const result = await Article
    .find(filter)
    .skip(offset)
    .limit(limit)
    .sort({ createdAt: -1 })
    .populate('category')

  return result;
}

const getArticle = async (data) => {
  const id = getObjectId(data)
  const result = await Article
    .findOne({ _id: id })
    .populate('category')
  
  return result && result.toObject()
}

const increaseArticleViews = async (data) => {
  const id = getObjectId(data)
  const result = await Article
    .findOneAndUpdate(
      { _id: id },
      {
        $inc: { view: 1}
      },
      {
        new: true,
      }
    )
  
  return result && result.toObject()
}

const addRating = async (article, rating) => {
  const id = getObjectId(article)

  const commentCount = await CommentService.countArticleComments({
    article: id,
    rating: {
      $gt: 0,
      $lte: 5
    }
  })
  const currentArticle = (await Article.findOne({ _id: id }).select('sumRating')).toObject()
  const currentRating = currentArticle?.sumRating

  const newRating = (currentRating * (commentCount - 1) + rating) / (commentCount)
  const result = await Article.updateOne(
    { _id: id},
    {
      $set: {
        sumRating: newRating
      }
    }
  )

  return result
}

module.exports = {
  upsertArticle,
  removeArticle,
  fetchArticle,
  getArticle,
  increaseArticleViews,
  addRating,
}
const { ObjectId } = require("mongodb")
const { ValidateException } = require("../exceptions")
const { Comment } = require("../models")
const { getObjectId } = require("../utils/common-util")

const upsertComment = async (data) => {
  data._id = data._id || new ObjectId()

  data = await Comment.findOneAndUpdate(
    { _id: data._id },
    {
      $set: data
    },
    {
      upsert: true,
      new: true,
    }
  )

  return data && data.toObject()
}

const removeComment = async (data) => {
  const id = getObjectId(data)
  const result = await Comment.remove({ _id: id })
  return result
}

const fetchComment = async ({ article, offset, limit }) => {
  const articleId = getObjectId(article)
  if (!articleId) {
    throw new ValidateException('Article is empty')
  }
  
  let filter = { article: articleId }

  offset = offset || 0
  limit = limit || 100

  const result = await Comment
    .find(filter)
    .populate('user')
    .skip(offset)
    .limit(limit)
    .sort({ createdAt: -1 })

  return result
}

const countArticleComments = async (filter) => {
  const result = await Comment.count(filter)

  return result
}

module.exports = {
  upsertComment,
  removeComment,
  fetchComment,
  countArticleComments,
}
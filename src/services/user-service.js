const { AppException, AuthenException } = require("../exceptions");
const bcrypt = require('bcrypt');
const { User } = require("../models");
const jwt = require('jsonwebtoken')
const config = require('config');

const registerToken = (data) => {
  const token = jwt.sign(data, config.tokenSecret, {
    expiresIn: config.tokenExpires
  })

  return token
}

const signUp = async (data) => {
  const { email } = data;

  const emailUserSearch = await User.findOne({ email: email })
  if (emailUserSearch && emailUserSearch._id)
    throw new AppException('Email already in use');

  // Gen hash password
  const salt = await bcrypt.genSalt(12);
  const hashPass = await bcrypt.hash(data.password, salt);

  // Update new hash password
  data.password = hashPass;

  // Insert new account to DB
  const user = new User(data)
  const result = await user.save()
  // const result = await User.insertMany([accountData]);
  
  // Remove unecessary fields and return result
  if (result) {
    const account = result.toObject()

    delete account.password

    const token = registerToken(account)

    return { ...account, token }
  }

  throw new AppException('Something went wrong!')
}

const login = async (data) => {
  let { email, password } = data
  let result = await User.findOne({ email: email }).select('+password');

  // Do login - Compare password
  if (result) {
    let user = result.toObject();

    let valid = user?.password 
      && await bcrypt.compare(password, user.password)
      || false;    

    if (valid) {
      delete user.password;

      let token = registerToken(user)

      return { ...user, token }
    }

    throw new AuthenException('Please recheck your information.');
  }

  // If phone does not exist - Do sign up
  return await signUp(data)
}

const getProfile = async (userId) => {
  const result = await User.findOne({ _id: userId });
  delete result?.password

  return result
}

const updateProfile = async (data, user) => {
  const { _id, email } = data
  if (_id !== user._id && email !== user.email) 
    throw new AppException('Something went wrong')

  const salt = await bcrypt.genSalt(12);
  const hashPass = await bcrypt.hash(data.password, salt);

  // Update new hash password
  data.password = hashPass;

  const filter = (_id && { _id }) 
    || (email && { email })
    || {}
  
  const result = await User.findOneAndUpdate(
    filter,
    {
      $set: data
    },
    { new: true }
  )

  if (!result) {
    throw new AppException('Update profile failed')
  }

  return result;
}

module.exports = {
  signUp,
  login,
  getProfile,
  updateProfile,
}
const UserService = require('./user-service')
const FileService = require('./file-service')
const ArticleService = require('./article-service')
const CategoryService = require('./category-service')
const CommentService = require('./comment-service')

module.exports = {
  UserService,
  FileService,
  ArticleService,
  CategoryService,
  CommentService
}
const { Media } = require("../models")
const { MediaService, FileService } = require("../services")
const formidable = require('formidable')
const { ValidateException } = require("../exceptions")
const catchAsync = require("../utils/catchAsync")

const uploadFile = catchAsync(async (req, res, next) => {
  const form = new formidable.IncomingForm()
  const file = await new Promise(async (resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) reject(err)

      const file = files.file
      resolve(file)
    })
  })
  if (!file) throw new ValidateException('File')

  const result = await FileService.upload(file)
  res.json({
    data: result
  })
})

module.exports = {
  uploadFile,
}
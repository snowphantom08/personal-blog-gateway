const { ArticleService } = require("../services")
const catchAsync = require("../utils/catchAsync")

const searchArticle = catchAsync(async (req, res, next) => {
  const data = req.query
  const result = await ArticleService.fetchArticle(data)

  res.json({
    data: result
  })
})

const newArticle = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await ArticleService.upsertArticle(data)

  res.json({
    data: result,
  })
})

const updateArticle = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await ArticleService.upsertArticle(data)

  res.json({
    data: result,
  })
})

const removeArticle = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await ArticleService.removeArticle(data)

  res.json({
    data: result
  })
})

const getArticle = catchAsync(async (req, res, next) => {
  const data = req.params
  const result = await ArticleService.getArticle(data)

  res.json({
    data: result
  })
})

const viewArticle = catchAsync(async (req, res, next) => {
  const data = req.params
  const result = await ArticleService.increaseArticleViews(data)

  res.json({
    data: result
  })
})

module.exports = {
  newArticle,
  updateArticle,
  removeArticle,
  searchArticle,
  getArticle,
  viewArticle,
}

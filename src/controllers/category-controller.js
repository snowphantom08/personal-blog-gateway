const { CategoryService } = require("../services")
const catchAsync = require("../utils/catchAsync")

const searchCategory = catchAsync(async (req, res, next) => {
  const data = req.query
  const result = await CategoryService.fetchCategory(data)

  res.json({
    data: result
  })
})

const newCategory = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await CategoryService.upsertCategory(data)

  res.json({
    data: result,
  })
})

const updateCategory = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await CategoryService.upsertCategory(data)

  res.json({
    data: result,
  })
})

const removeCategory = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await CategoryService.removeCategory(data)

  res.json({
    data: result
  })
})

const getCategory = catchAsync(async (req, res, next) => {
  const data = req.params
  const result = await CategoryService.getCategory(data)

  res.json({
    data: result
  })
})

module.exports = {
  newCategory,
  updateCategory,
  removeCategory,
  searchCategory,
  getCategory,
}

const UserController = require('./user-controller')
const ArticleController = require('./article-controller')
const CategoryController = require('./category-controller')
const CommentController = require('./comment-controller')
const FileController = require('./file-controller')

module.exports = {
  UserController,
  ArticleController,
  CategoryController,
  CommentController,
  FileController,
}
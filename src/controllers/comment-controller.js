const { CommentService, ArticleService } = require("../services")
const catchAsync = require("../utils/catchAsync")
const { getObjectId } = require("../utils/common-util")

const newComment = catchAsync(async (req, res, next) => {
  const data = req.body
  const user = req.user
  const result = await CommentService.upsertComment({
    ...data,
    user: user
  })

  res.json({
    data: result
  })

  if (result.rating > 0 && result.rating <= 5) {
    await ArticleService.addRating({ _id: getObjectId(result.article) }, result.rating)
  }
})

const fetchComment = catchAsync(async (req, res, next) => {
  const data = req.query
  const result = await CommentService.fetchComment(data)

  res.json({
    data: result
  })
})

const updateComment = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await CommentService.upsertComment(data)

  res.json({
    data: result
  })
})

const removeComment = catchAsync(async (req, res, next) => {
  const data = req.body
  const result = await CommentService.removeComment(data)

  res.json({
    data: result
  })
})

module.exports = {
  fetchComment,
  newComment,
  updateComment,
  removeComment
}
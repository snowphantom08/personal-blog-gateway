const { UserService } = require("../services")
const catchAsync = require('../utils/catchAsync')

const signUp = catchAsync(async (req, res) => {
  let result = await UserService.signUp(req.body);
  res.json({
    data: result
  })
})

const login = catchAsync(async (req, res) => {
  let result = await UserService.login(req.body);
  res.json({
    data: result
  })
})

const updateProfile = catchAsync(async (req, res) => {
  let result = await UserService.updateProfile(req.body, req.user)
  res.json({
    data: result
  })
})

const getProfile = catchAsync(async (req, res) => {
  let result = req.user;
  res.json({
    data: result
  })
})

module.exports = {
  signUp,
  login,
  updateProfile,
  getProfile
}
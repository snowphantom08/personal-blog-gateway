# Personal Blog Gateway API
This is Personal Blog Gateway API project.

# How to run

- Clone this project
- Install package: npm install
- Run Develop: npm run dev

# Run production

- Run Product: npm start

# Contributor

- Hung Nguyen
- Khu Tran

# License

- MIT License
process.env.NODE_CONFIG_DIR = './src/config';
const fs = require('fs');
const config = require('config');
const express = require('express');
const cors = require('cors');
var http = require('http');
const { NotFoundException } = require('./src/exceptions')
const { ErrorHandleMiddleware, ErrorLogsMiddleware } = require('./src/middlewares')

//#region IMPORT ROUTER
const routes = require('./src/routes/index');
const { connectDB } = require('./src/infrastructure/db-manager');
//#endregion

const app = express();
var server = http.createServer(app);

// Create logs folder if not exist

//#region SET UP MIDDLEWARE
app.use(cors(config.app.cors));
app.use(express.json({ limit: '100kb' }));
app.use(express.urlencoded({ limit: '100kb', extended: true }));
//#endregion

// Connect to DB
connectDB()

//#region SET UP ROUTER
app.use('/api/v1', routes);
//#endregion

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new NotFoundException('Not Found!'));
});

app.use([
  ErrorLogsMiddleware,
  ErrorHandleMiddleware
])

const port = config.port || process.env.PORT || 5000
const host = config.host || process.env.HOST || 'localhost'
server.listen(port, () =>
  console.log(`Server running on ${host}:${port}`)
);

require('./src/infrastructure/app-manager')(app)
